﻿using System;
using System.IO;

namespace Set1._2
{
    class Program
    {
        public static string XorHexStrings(string hexString1, string hexString2)
        {
            byte[] hex1 = new byte[hexString1.Length / 2];
            byte[] hex2 = new byte[hexString2.Length / 2];
            byte[] result = new byte[hexString2.Length / 2];

            for (int i = 0; i < hexString1.Length; i++)
            {
                hex1[i / 2] = Convert.ToByte(Convert.ToInt32(hexString1.Substring(i, 2), 16));
                hex2[i / 2] = Convert.ToByte(Convert.ToInt32(hexString2.Substring(i, 2), 16));

                result[i / 2] = Convert.ToByte(hex1[i / 2] ^ hex2[i / 2]);
                i += 1;
            }

            string res = BitConverter.ToString(result);
            return res.Replace("-", "");
        }

        static void Main(string[] args)
        {
            string result;
            string answer;
            string hexString1;
            string hexString2;

            using (StreamReader stream = new StreamReader("Data/hex1.txt"))
            {
                hexString1 = stream.ReadLine();
            }

            using (StreamReader stream = new StreamReader("Data/hex2.txt"))
            {
                hexString2 = stream.ReadLine();
            }

            using (StreamReader stream = new StreamReader("Data/answer.txt"))
            {
                answer = stream.ReadLine();
            }

            result = XorHexStrings(hexString1, hexString2);

            System.Diagnostics.Debug.Assert(result == answer.ToUpper(), "Wrong algorithm.");

            Console.WriteLine(result);
            Console.ReadKey();
        }
    }
}
