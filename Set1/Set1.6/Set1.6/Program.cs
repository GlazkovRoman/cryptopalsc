﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Set1._6
{
    class Program
    {
        private static string ConvertBase64ToHexString(string str)
        {
            byte[] bytes = Convert.FromBase64String(str);

            string res = BitConverter.ToString(bytes);
            return res.Replace("-", "");
        }

        private static string TranslateHexStringToText(string hexString)
        {
            byte[] hex = new byte[hexString.Length / 2];
            string result = "";

            for (int i = 0; i < hexString.Length; i++)
            {
                hex[i / 2] = Convert.ToByte(Convert.ToInt32(hexString.Substring(i, 2), 16));
                result += Convert.ToChar(hex[i / 2]);
                i += 1;
            }

            return result;
        }

        private static List<int> FindKeySize(string text, int countKeys = 1, int minKeySize = 2, int maxKeySize = 40)
        {
            string help;
            string subString1;
            string subString2;
            string subString3;
            string subString4;

            double hammingDistance = 0;

            List<int> result = new List<int>();
            Dictionary<int, double> dictionary = new Dictionary<int, double>();
            Dictionary<int, double> dictionaryHelp = new Dictionary<int, double>();

            if (minKeySize > text.Length)
            {
                return null;
            }

            for (int keyLen = minKeySize; keyLen <= maxKeySize; keyLen++)
            {
                hammingDistance = 0;

                if (text.Length / keyLen < 4)
                {
                    help = "";
                    subString1 = text.Substring(0, keyLen);
                    subString2 = text.Substring(keyLen);

                    for (int j = 0; j < keyLen - (text.Length - keyLen); j++)
                    {
                        help += Convert.ToChar(0);
                    }

                    help += subString2;
                    subString2 = help;
                    hammingDistance += (double) HammingDistance(subString1, subString2) / (double) keyLen;
                }
                else
                {
                    subString1 = text.Substring(0, keyLen);
                    subString2 = text.Substring(keyLen, keyLen);
                    subString3 = text.Substring(keyLen * 2, keyLen);
                    subString4 = text.Substring(keyLen * 3, keyLen);

                    hammingDistance += (double) HammingDistance(subString1, subString2) / (double) keyLen;
                    hammingDistance += (double) HammingDistance(subString1, subString3) / (double) keyLen;
                    hammingDistance += (double) HammingDistance(subString1, subString4) / (double) keyLen;

                    hammingDistance += (double) HammingDistance(subString2, subString3) / (double) keyLen;
                    hammingDistance += (double) HammingDistance(subString2, subString4) / (double) keyLen;
                    hammingDistance += (double) HammingDistance(subString3, subString4) / (double) keyLen;
                }

                dictionary.Add(keyLen, hammingDistance);
            }

            foreach (KeyValuePair<int, double> pair in dictionary.OrderBy(key => key.Value))
            {
                dictionaryHelp.Add(pair.Key, pair.Value);
            }

            if (countKeys > dictionary.Count)
            {
                countKeys = dictionary.Count;
            }

            for (int i = 0; i < countKeys; i++)
            {
                result.Add(dictionaryHelp.Keys.ElementAt(i));
            }

            return result;
        }

        private static string ConvertStringToBitsString(string str)
        {
            string help1;
            string help2;
            string result = "";

            for (int i = 0; i < str.Length; i++)
            {
                help1 = Convert.ToString(str[i], 2);

                if (help1.Length < 8)
                {
                    help2 = "";

                    for (int j = help1.Length; j < 8; j++)
                    {
                        help2 += "0";
                    }

                    help2 += help1;
                    help1 = help2;
                }

                result += help1;
            }
            
            return result;
        }

        private static int HammingDistance(string str1, string str2)
        {
            int distance = 0;
            string bitsString1 = ConvertStringToBitsString(str1);
            string bitsString2 = ConvertStringToBitsString(str2);

            for (int i = 0; i < bitsString1.Length; i++)
            {
                if (bitsString1[i] != bitsString2[i]) distance += 1;
            }

            return distance;
        }

        private static int IsCoolString(string str)
        {
            for (int i = 0; i < str.Length; i++)
            {
                if ((str[i] > 127 || str[i] < 32) && str[i] != 10 && str[i] != 13)
                {
                    return i;
                }
            }

            return -1;
        }

        private static string RepeatingKeyXor(string text, string key, bool makeTab = false)
        {
            string result = "";

            for (int i = 0; i < text.Length; i++)
            {
                if (makeTab && (i % key.Length) == 0 && i != 0 && i != text.Length - 1)
                {
                    result += " :: ";
                }

                if (key[i % key.Length] == 0)
                {
                    result += text[i];
                }
                else
                {
                    result += Convert.ToChar(text[i] ^ key[i % key.Length]);
                }
            }

            return result;
        }

        private static List<string> FindGoodKeyMeaning(string text, int keySize,
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 :")
        {
            string help = "";
            string partOfText = "";
            string strForResult = "";
            List<string> result = new List<string>();

            for (int currentKeySymbol = 0; currentKeySymbol < keySize; currentKeySymbol++)
            {
                partOfText = "";
                strForResult = "";

                for (int i = currentKeySymbol; i < text.Length; i += keySize)
                {
                    partOfText += text[i];
                }

                for (int i = 0; i < chars.Length; i++)
                {
                    help = RepeatingKeyXor(partOfText, chars[i].ToString());

                    if (IsCoolString(help) == -1)
                    {
                        strForResult += chars[i];
                    }
                }

                result.Add(strForResult);
            }

            return result;
        }

        private static int BruteforceKey(string text, int keySize, StreamWriter stream, List<string> chars = null, int currentKeySymbol = 0,
            string currentKey = "", bool makeTab = false)
        {
            if (currentKey == "")
            {
                currentKey = new string('0', keySize);
            }

            if (chars == null)
            {
                chars = new List<string>();

                for (int i = 0; i < keySize; i++)
                {
                    chars.Add("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 :");
                }
            }

            if (currentKeySymbol < keySize)
            {
                int wasMistakeIn = -1;

                if (chars[currentKeySymbol] != "")
                {
                    for (int keyChar = 0; keyChar < chars[currentKeySymbol].Length; keyChar++)
                    {
                        char[] help = currentKey.ToCharArray();

                        help[currentKeySymbol] = chars[currentKeySymbol][keyChar];
                        currentKey = string.Join("", help);

                        wasMistakeIn = BruteforceKey(text, keySize, stream, chars, currentKeySymbol + 1, currentKey, makeTab);

                        if (wasMistakeIn != -1 && wasMistakeIn != currentKeySymbol)
                        {
                            return wasMistakeIn;
                        }
                    }
                }
                else
                {
                    char[] help = currentKey.ToCharArray();

                    help[currentKeySymbol] = Convert.ToChar(0);
                    currentKey = string.Join("", help);

                    wasMistakeIn = BruteforceKey(text, keySize, stream, chars, currentKeySymbol + 1, currentKey, makeTab);

                    if (wasMistakeIn != -1 && wasMistakeIn != currentKeySymbol)
                    {
                        return wasMistakeIn;
                    }
                }
            }
            else
            {
                string printText = RepeatingKeyXor(text, currentKey, makeTab);
                int wasMistakeIn = IsCoolString(printText);

                if (wasMistakeIn == -1)
                {
                    stream.WriteLine("----------" + currentKey + "----------\n\n" + printText + "\n");
                    return wasMistakeIn;
                }

                return wasMistakeIn % keySize;
            }

            return -1;
        }

        static void Main(string[] args)
        {
            int keySize;
            string help;
            string cipher = "";
            List<int> keySizeList = new List<int>();

            using (StreamReader stream = new StreamReader("Data/cipher.txt"))
            {
                while ((help = stream.ReadLine()) != null)
                {
                    cipher += help;
                }

                cipher = TranslateHexStringToText(ConvertBase64ToHexString(cipher));
            }

            keySizeList = FindKeySize(cipher, 5);
            keySize = keySizeList[0];

            using (StreamWriter stream = new StreamWriter("Data/bruteforceKey.txt"))
            {
                /*var keyMeanings = FindGoodKeyMeaning(cipher, keySize);

                string text = "";
                List<string> list = new List<string>();

                int step = 6;
                int start = 23;

                for (int i = start; i < cipher.Length; i += keySize)
                {
                    if (cipher.Length - i > step)
                    {
                        text += cipher.Substring(i, step);
                    }
                    else
                    {
                        text += cipher.Substring(i);
                    }
                }

                for (int i = start; i < start + step; i++)
                {
                    list.Add(keyMeanings[i]);
                }

                BruteforceKey(text, step, stream, list, makeTab: true);*/

                Console.WriteLine("Key : Terminator X: Bring the noise\n\nText : \n\n" + RepeatingKeyXor(cipher, "Terminator X: Bring the noise"));
                Console.ReadKey();
            }
        }
    }
}
