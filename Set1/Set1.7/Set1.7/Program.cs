﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Set1._7
{
    class Program
    {
        private static string DecryptStringFromBytesAes(byte[] cipherText, byte[] key)
        {
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (key == null || key.Length <= 0)
                throw new ArgumentNullException("key");
            
            string plaintext = null;
            
            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.Key = key;
                aesAlg.Mode = CipherMode.ECB;
                aesAlg.Padding = PaddingMode.Zeros;

                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, null);
                
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }

            return plaintext;
        }

        static void Main(string[] args)
        {
            using (StreamWriter streamWriter = new StreamWriter("Data/answer.txt"))
            {
                string cipher = null;

                using (StreamReader streamReader = new StreamReader("Data/cipher.txt"))
                {
                    string help = null;

                    while ((help = streamReader.ReadLine()) != null)
                    {
                        cipher += help;
                    }
                }

                string plaintext = DecryptStringFromBytesAes(Convert.FromBase64String(cipher), Encoding.ASCII.GetBytes("YELLOW SUBMARINE"));

                streamWriter.WriteLine(plaintext);

                Console.WriteLine(plaintext);
                Console.ReadKey();
            }
        }
    }
}
