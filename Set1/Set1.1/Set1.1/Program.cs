﻿using System;
using System.IO;

namespace Set1._1
{
    class Program
    {
        public static string ConvertBase64ToString(string str)
        {
            byte[] bytes = Convert.FromBase64String(str);

            string res = BitConverter.ToString(bytes);
            return res.Replace("-", "");
        }

        public static string ConvertHexStringToBase64(string hexString)
        {
            byte[] buffer = new byte[hexString.Length / 2];

            for (int i = 0; i < hexString.Length; i++)
            {
                buffer[i / 2] = Convert.ToByte(Convert.ToInt32(hexString.Substring(i, 2), 16));
                i += 1;
            }

            string res = Convert.ToBase64String(buffer);
            return res;
        }

        static void Main(string[] args)
        {
            string hex;
            string answer;

            using (StreamReader stream = new StreamReader("Data/hex.txt"))
            {
                hex = stream.ReadLine();
                hex = ConvertHexStringToBase64(hex);
            }

            using (StreamReader stream = new StreamReader("Data/answer.txt"))
            {
                answer = stream.ReadLine();
            }

            using (StreamWriter stream = new StreamWriter("Data/base64.txt"))
            {
                stream.WriteLine(hex);
            }

            System.Diagnostics.Debug.Assert(answer == hex, "Wrong algorithm.");
            
            Console.WriteLine(hex);
            Console.ReadKey();
        }
    }
}
