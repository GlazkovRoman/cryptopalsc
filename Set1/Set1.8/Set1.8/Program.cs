﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Set1._8
{
    class Program
    {
        private static List<int> DetectEcbModeInHexString(List<string> textList)
        {
            int step = 32;
            string help1 = null;
            string help2 = null;
            List<int> result = new List<int>();

            for (int i = 0; i < textList.Count; i++)
            {
                for (int j = 0; j < textList[i].Length; j += step)
                {
                    if (textList[i].Length - j > step)
                    {
                        help1 = textList[i].Substring(j, step);
                        help2 = textList[i].Substring(j + step);

                        if (help2.IndexOf(help1) != -1)
                        {
                            if (!result.Exists(x => x == i)) result.Add(i);
                        }
                    }
                }
            }

            return result;
        }

        static void Main(string[] args)
        {
            List<string> textList = new List<string>();

            using (StreamReader streamReader = new StreamReader("Data/cipher.txt"))
            {
                string help = null;

                while ((help = streamReader.ReadLine()) != null)
                {
                    textList.Add(help);
                }
            }

            var ecbModeStrings = DetectEcbModeInHexString(textList);

            foreach (var num in ecbModeStrings)
            {
                Console.Write($"Number: {num}\n\nString: {textList[num]}\n\n");
            }

            Console.ReadKey();
        }
    }
}
