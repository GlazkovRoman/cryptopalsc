﻿using System;
using System.IO;
using System.Text;

namespace Set1._5
{
    class Program
    {
        public static string StringToHex(string str)
        {
            byte[] bytes = Encoding.Default.GetBytes(str);
            string hexString = BitConverter.ToString(bytes);

            return hexString = hexString.Replace("-", "");
        }

        public static string RepeatingKeyXor(string text, string key)
        {
            string result = "";

            for (int i = 0; i < text.Length; i++)
            {
                result += Convert.ToChar(text[i] ^ key[i % key.Length]);
            }

            return result;
        }

        static void Main(string[] args)
        {
            string text;
            string cipher;
            string answer = "";
            string plaintext = "";

            using (StreamReader stream = new StreamReader("Data/answer.txt"))
            {
                while ((text = stream.ReadLine()) != null)
                {
                    answer += text;
                }
            }

            using (StreamReader stream = new StreamReader("Data/text.txt"))
            {
                while ((text = stream.ReadLine()) != null)
                {
                    plaintext += text;
                    plaintext += "\n";
                }

                plaintext = plaintext.Substring(0, plaintext.Length - 1);

                cipher = RepeatingKeyXor(plaintext, "ICE");
                cipher = StringToHex(cipher);
            }

            using (StreamWriter stream = new StreamWriter("Data/cipher.txt"))
            {
                stream.WriteLine(cipher);
            }

            System.Diagnostics.Debug.Assert(cipher == answer.ToUpper(), "Wrong algorithm.");

            Console.WriteLine(cipher);
            Console.ReadKey();
        }
    }
}
