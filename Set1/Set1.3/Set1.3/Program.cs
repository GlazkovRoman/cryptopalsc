﻿using System;
using System.IO;

namespace Set1._3
{
    class Program
    {
        public static string XorHexStringAndChar(string hexString, int ch)
        {
            byte[] hex = new byte[hexString.Length / 2];
            string result = "";
            
            for (int i = 0; i < hexString.Length; i++)
            {
                hex[i / 2] = Convert.ToByte(Convert.ToInt32(hexString.Substring(i, 2), 16));
                result += Convert.ToChar(hex[i / 2] ^ ch);
                i += 1;
            }
            
            return result;
        }

        static void Main(string[] args)
        {
            string hex;
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

            using (StreamReader stream = new StreamReader("Data/hex.txt"))
            {
                hex = stream.ReadLine();
            }

            using (StreamWriter stream = new StreamWriter("Data/result.txt"))
            {
                for (int i = 0; i < chars.Length; i++)
                {
                    stream.WriteLine(chars[i] + " : " + XorHexStringAndChar(hex, chars[i]));
                }
            }
        }
    }
}
