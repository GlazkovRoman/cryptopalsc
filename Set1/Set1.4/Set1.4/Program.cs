﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Set1._4
{
    class Program
    {
        public static string TranslateHexToText(string hexString)
        {
            byte[] hex = new byte[hexString.Length / 2];
            string result = "";

            for (int i = 0; i < hexString.Length; i++)
            {
                hex[i / 2] = Convert.ToByte(Convert.ToInt32(hexString.Substring(i, 2), 16));
                result += Convert.ToChar(hex[i / 2]);
                i += 1;
            }

            return result;
        }

        public static bool IsCoolString(string str)
        {
            for (int i = 0; i < str.Length; i++)
            {
                if ((str[i] > 127 || str[i] < 32) && str[i] != 10)
                {
                    return false;
                }
            }

            return true;
        }

        public static string XorHexStringAndChar(string hexString, int ch)
        {
            byte[] hex = new byte[hexString.Length / 2];
            string result = "";

            for (int i = 0; i < hexString.Length; i++)
            {
                hex[i / 2] = Convert.ToByte(Convert.ToInt32(hexString.Substring(i, 2), 16));
                result += Convert.ToChar(hex[i / 2] ^ ch);
                i += 1;
            }

            return result;
        }

        public static List<string> XorHexStringOnChars(string hexString,
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890")
        {
            string str;
            string help;
            List<string> stringList = new List<string>();

            for (int i = 0; i < chars.Length; i++)
            {
                str = chars[i] + " :: ";
                help = XorHexStringAndChar(hexString, chars[i]);

                if (IsCoolString(help))
                {
                    str += help;
                    stringList.Add(str);
                }
            }

            return stringList;
        }

        static void Main(string[] args)
        {
            string hexString;

            #region See encrypted strings

            /*using (StreamReader streamR = new StreamReader("Data/hexText.txt"))
            {
                using (StreamWriter streamW = new StreamWriter("Data/normalText.txt"))
                {
                    string help;

                    while ((help = streamR.ReadLine()) != null)
                    {
                        hexString = help;
                        streamW.WriteLine(TranslateHexToText(hexString));
                    }
                }
            }*/

            #endregion

            using (StreamReader streamR = new StreamReader("Data/hexText.txt"))
            {
                using (StreamWriter streamW = new StreamWriter("Data/findString.txt"))
                {
                    int i = 1; //Номер строки
                    string help;
                    List<string> stringList = new List<string>();

                    while ((help = streamR.ReadLine()) != null)
                    {
                        stringList = XorHexStringOnChars(help);

                        if (stringList.Count != 0)
                        {
                            streamW.WriteLine(i.ToString() + "------------------------------------------");
                            streamW.WriteLine();

                            foreach (var str in stringList)
                            {
                                streamW.WriteLine(str);
                            }

                            streamW.WriteLine();
                        }

                        i += 1;
                    }
                }
            }
        }
    }
}
