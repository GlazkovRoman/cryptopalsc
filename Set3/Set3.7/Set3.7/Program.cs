﻿using System;
using System.Globalization;

namespace Set3._7
{
    class Program
    {
        private class MT19937
        {
            private const int U = 11;

            private const int S = 7;

            private const int T = 15;

            private const int L = 18;

            private const uint N = 624;

            private const uint M = 397;

            private const uint A = 0x9908B0DFU; //Последняя строка A[]

            private const uint B = 0x9D2C5680U; //Специально подобранная маска

            private const uint C = 0xEFC60000U; //Специально подобранная маска

            private uint _nextRand;

            private int _stepsToReload = -1;

            private uint[] _state = new uint[N]; //Внутреннее состояние регистра

            private uint HighestBit(uint number)
            {
                return ((number) & 0x80000000U);
            }

            private uint LowestBit(uint number)
            {
                return ((number) & 0x00000001U);
            }

            private uint LowestBits(uint number)
            {
                return ((number) & 0x7FFFFFFFU);
            }

            private uint MixBits(uint numberU, uint numberV)
            {
                return (HighestBit(numberU) | LowestBits(numberV));
            }

            private uint Strongest(uint number) //Закалка
            {
                number ^= (number >> U);
                number ^= (number << S) & B;
                number ^= (number << T) & C;

                return (number ^ (number >> L));
            }

            private uint Reload() //Перемешивание внутреннего состояния регистра
            {
                int j;
                uint s0;
                uint s1;
                uint p0 = 0;
                uint p2 = 2;
                uint pM = M;

                if (_stepsToReload < -1)
                {
                    var help = DateTime.Now.ToString(new CultureInfo("ru-RU")).Split(new char[] { ':', ' ', '.' });
                    Seed(Convert.ToUInt32(help[0] + help[1] + help[3] + help[4] + help[5]));
                }

                for (s0 = _state[0], s1 = _state[1], j = (int)(N - M); Convert.ToBoolean(--j); s0 = s1, s1 = _state[p2++])
                {
                    _state[p0++] = _state[pM++] ^ (MixBits(s0, s1) >> 1) ^ (Convert.ToBoolean(LowestBit(s1)) ? A : 0U);
                }

                for (pM = 0, j = (int)M; Convert.ToBoolean(--j); s0 = s1, s1 = _state[p2++])
                {
                    _state[p0++] = _state[pM++] ^ (MixBits(s0, s1) >> 1) ^ (Convert.ToBoolean(LowestBit(s1)) ? A : 0U);
                }

                _nextRand = 0;
                _stepsToReload = (int)(N - 1);

                s1 = _state[0];
                _state[p0] = _state[pM] ^ (MixBits(s0, s1) >> 1) ^ (Convert.ToBoolean(LowestBit(s1)) ? A : 0U);

                return Strongest(s1);
            }

            public MT19937()
            {
                var help = DateTime.Now.ToString(new CultureInfo("ru-RU")).Split(new char[] { ':', ' ', '.' });
                Seed(Convert.ToUInt32(help[0] + help[1] + help[3] + help[4] + help[5]));
            }

            public MT19937(uint seed)
            {
                Seed(seed);
            }

            public uint StateLength
            {
                get { return N; }

                private set { }
            }

            public uint[] State
            {
                get { return _state; }

                set
                {
                    _stepsToReload = 0;

                    for (int i = 0; i < value.Length; i++)
                    {
                        _state[i] = value[i];
                    }
                }
            }

            public void Seed(uint seed)
            {
                int j;
                int i = 0;
                uint x = seed & 0xFFFFFFFFU;

                for (_stepsToReload = 0, _state[i++] = x, j = (int)N; Convert.ToBoolean(--j); _state[i++] = (x *= 69069U) & 0xFFFFFFFFU) ;
            }

            public uint Random()
            {
                if (--_stepsToReload < 0)
                {
                    return Reload();
                }

                _nextRand = (_nextRand + 1) % N;

                return Strongest(_state[_nextRand]);
            }
        }

        private static uint UnStrongest(uint number)
        {
            uint help = 0;
            uint result = 0;

            number ^= (number >> 18);
            number ^= (number << 15) & 4022730752;
            
            help = number ^ ((number << 7) & 2636928640);
            help = number ^ ((help << 7) & 2636928640);
            help = number ^ ((help << 7) & 2636928640);
            help = number ^ ((help << 7) & 2636928640);
            
            result = help ^ (help >> 11);
            result = result >> 11;
            result = help ^ result;

            return result;
        }

        static void Main(string[] args)
        {
            MT19937 mt19937 = new MT19937();
            uint[] mtRandomNumbers = new uint[mt19937.StateLength];

            for (int i = 0; i < mt19937.StateLength; i++)
            {
                mtRandomNumbers[i] = mt19937.Random();
            }

            var mtState = mt19937.State;
            var decodedState = new uint[mt19937.StateLength];

            for (int i = 0; i < mt19937.StateLength; i++)
            {
                decodedState[i] = UnStrongest(mtRandomNumbers[i]);
                System.Diagnostics.Debug.Assert(mtState[i] == decodedState[i], "Wrong algorithm!");
            }

            Console.WriteLine("Program has been finished.");
            Console.ReadKey();
        }
    }
}
