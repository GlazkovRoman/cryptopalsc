﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Set3._1
{
    class Program
    {
        static private AesManaged _aesAlg = new AesManaged();

        private static string PaddingPKCS7(string message, int blockSize = 16)
        {
            int padding = 0;
            string result = message;

            padding = blockSize - (message.Length % blockSize);

            for (int i = 0; i < padding; i++)
            {
                result += Convert.ToChar(padding);
            }

            return result;
        }

        private static bool PaddingValidation(string text)
        {
            int padding = 0;
            string result = text;

            padding = result[result.Length - 1];

            if (padding == 0)
            {
                return false;
            }

            for (int i = result.Length - 1; i > (result.Length - 1) - padding; i--)
            {
                if (result[i] != Convert.ToChar(padding))
                {
                    return false;
                }
            }

            return true;
        }

        static byte[] EncryptStringToBytesAes(string plaintext)
        {
            if (plaintext == null || plaintext.Length <= 0)
                throw new ArgumentNullException("plaintext");

            byte[] encrypted;

            _aesAlg.Mode = CipherMode.CBC;
            _aesAlg.Padding = PaddingMode.None;

            ICryptoTransform encryptor = _aesAlg.CreateEncryptor(_aesAlg.Key, _aesAlg.IV);

            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                    {
                        swEncrypt.Write(plaintext);
                    }

                    encrypted = msEncrypt.ToArray();
                }
            }

            return encrypted;
        }

        private static string DecryptStringFromBytesAes(byte[] cipherText)
        {
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");

            string plaintext = null;

            _aesAlg.Mode = CipherMode.CBC;
            _aesAlg.Padding = PaddingMode.None;

            ICryptoTransform decryptor = _aesAlg.CreateDecryptor(_aesAlg.Key, _aesAlg.IV);

            using (MemoryStream msDecrypt = new MemoryStream(cipherText))
            {
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                {
                    using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                    {
                        plaintext = srDecrypt.ReadToEnd();
                    }
                }
            }

            return plaintext;
        }

        private static KeyValuePair<byte[], byte[]> EncryptRandomText(List<string> textList)
        {
            Random random = new Random();
            string text = textList[random.Next() % textList.Count];
            return new KeyValuePair<byte[], byte[]>(_aesAlg.IV, EncryptStringToBytesAes(PaddingPKCS7(text)));
        }

        private static bool RequestServer(byte[] cipherText)
        {
            string text = DecryptStringFromBytesAes(cipherText);
            return PaddingValidation(text);
        }

        private static string PaddingOracleAttack(byte[] changedBlock, byte[] decryptedBlock, int blockSize = 16)
        {
            string result = "";

            byte[] message = new byte[blockSize * 2];
            byte[] changedText = new byte[blockSize];
            byte[] intermediateState = new byte[blockSize];

            for (int i = blockSize - 1; i >= 0; i--)
            {
                for (int j = 0; j < blockSize; j++)
                {
                    changedText[j] = changedBlock[j];
                }

                for (int j = blockSize - 1; j > i; j--)
                {
                    changedText[j] = Convert.ToByte((blockSize - i) ^ intermediateState[j]);
                }

                for (int ch = 0; ch < 256; ch++)
                {
                    changedText[i] = Convert.ToByte(ch);

                    for (int j = 0; j < blockSize; j++)
                    {
                        message[j] = changedText[j];
                        message[j + blockSize] = decryptedBlock[j];
                    }

                    if (RequestServer(message))
                    {
                        if (i == blockSize - 1)
                        {
                            message[blockSize - 2] -= 1;

                            if (RequestServer(message))
                            {
                                intermediateState[i] = Convert.ToByte(changedText[i] ^ (blockSize - i));
                                break;
                            }
                        }
                        else
                        {
                            intermediateState[i] = Convert.ToByte(changedText[i] ^ (blockSize - i));
                            break;
                        }
                    }
                }
            }

            for (int i = 0; i < blockSize; i++)
            {
                result += Convert.ToChar(intermediateState[i] ^ changedBlock[i]);
            }

            return result;
        }

        private static string AttackOnCiphertext(KeyValuePair<byte[], byte[]> cipher, int blockSize = 16)
        {
            string help = "";
            string result = "";
            int numberOfBlocks = cipher.Value.Length / blockSize;

            byte[] changedBlock = new byte[blockSize];
            byte[] decryptedBlock = new byte[blockSize];

            for (int decryptedBlockNumber = numberOfBlocks - 1; decryptedBlockNumber >= 1; decryptedBlockNumber--)
            {
                for (int i = 0; i < blockSize; i++)
                {
                    decryptedBlock[i] = cipher.Value[i + decryptedBlockNumber * blockSize];
                    changedBlock[i] = cipher.Value[i + decryptedBlockNumber * blockSize - blockSize];
                }

                help = PaddingOracleAttack(changedBlock, decryptedBlock);
                result = help + result;
            }

            for (int i = 0; i < blockSize; i++)
            {
                decryptedBlock[i] = cipher.Value[i];
            }

            help = PaddingOracleAttack(cipher.Key, decryptedBlock);
            result = help + result;

            return result;
        }

        static void Main(string[] args)
        {
            List<string> textList = new List<string>();

            _aesAlg.KeySize = 128;
            _aesAlg.BlockSize = 128;

            _aesAlg.GenerateIV();
            _aesAlg.GenerateKey();

            using (StreamReader streamReader = new StreamReader("Data/strings.txt"))
            {
                string help = "";

                while ((help = streamReader.ReadLine()) != null)
                {
                    textList.Add(Encoding.ASCII.GetString(Convert.FromBase64String(help)));
                }
            }

            var cipher = EncryptRandomText(textList);

            Console.WriteLine($"Text: {DecryptStringFromBytesAes(cipher.Value)}");
            Console.WriteLine($"Cipher: {Encoding.ASCII.GetString(cipher.Value)}");
            Console.WriteLine($"Server answer: {RequestServer(cipher.Value)}\n");

            Console.WriteLine($"Text: {DecryptStringFromBytesAes(cipher.Value)}");
            Console.WriteLine($"Attack on ciphertext: {AttackOnCiphertext(cipher)}");

            Console.ReadKey();
        }
    }
}
