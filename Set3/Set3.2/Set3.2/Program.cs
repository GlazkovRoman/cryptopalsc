﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Set3._2
{
    class Program
    {
        private class CTRClass
        {
            private AesManaged _aesAlg = new AesManaged();

            private EncryptionDelegate Encryption = null;

            private ChangingCounterDelegate ChangingCounter = null;

            public delegate byte[] EncryptionDelegate(string plaintext);

            public delegate char[] ChangingCounterDelegate(char[] counter, int value);

            private byte[] RepeatingKeyXor(byte[] text, byte[] key)
            {
                byte[] result = new byte[text.Length];

                for (int i = 0; i < text.Length; i++)
                {
                    result[i] = Convert.ToByte(text[i] ^ key[i % key.Length]);
                }

                return result;
            }

            private void InitAes()
            {
                _aesAlg.KeySize = 128;
                _aesAlg.BlockSize = 128;
                _aesAlg.Mode = CipherMode.ECB;
                _aesAlg.Padding = PaddingMode.None;
                _aesAlg.Key = Encoding.ASCII.GetBytes("YELLOW SUBMARINE");
            }

            private byte[] EncryptStringToBytesAes(string plaintext)
            {
                if (plaintext == null || plaintext.Length <= 0)
                    throw new ArgumentNullException("plaintext");

                byte[] encrypted;
                
                ICryptoTransform encryptor = _aesAlg.CreateEncryptor(_aesAlg.Key, _aesAlg.IV);

                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(plaintext);
                        }

                        encrypted = msEncrypt.ToArray();
                    }
                }

                return encrypted;
            }

            private char[] CounterLastBlockCount(char[] counter, int value)
            {
                counter[8] = Convert.ToChar((counter[8] + value) % 256);
                return counter;
            }

            public CTRClass()
            {
                InitAes();

                Encryption = EncryptStringToBytesAes;
                ChangingCounter = CounterLastBlockCount;
            }

            public CTRClass(ChangingCounterDelegate changingCounterDelegate, EncryptionDelegate encryptionDelegate)
            {
                InitAes();

                Encryption = encryptionDelegate;
                ChangingCounter = changingCounterDelegate;
            }
            
            public byte[] Encrypt(byte[] text, int blockSize = 16)
            {
                int start = 0;
                int finish = 0;
                
                byte[] help = new byte[blockSize];
                byte[] result = new byte[text.Length];
                char[] counter = new string(Convert.ToChar(0), blockSize).ToCharArray();

                for (int i = 0; i < Math.Ceiling((double)text.Length / (double)blockSize); i++)
                {
                    byte[] counterCipher = Encryption(new string(counter));

                    if (i * blockSize + blockSize <= text.Length)
                    {
                        start = i * blockSize;
                        finish = i * blockSize + blockSize;
                    }
                    else
                    {
                        start = i * blockSize;
                        finish = text.Length;
                    }

                    help = new byte[finish - start];

                    for (int j = start; j < finish; j++)
                    {
                        help[j - i * blockSize] = text[j];
                    }

                    counter = ChangingCounter(counter, 1);
                    byte[] addToResult = RepeatingKeyXor(help, counterCipher);

                    for (int j = start; j < finish; j++)
                    {
                        result[j] = addToResult[j - i * blockSize];
                    }
                }

                return result;
            }

            public byte[] Decrypt(byte[] text, int blockSize = 16)
            {
                return Encrypt(text, blockSize);
            }
        }
        
        static void Main(string[] args)
        {
            string text = "";
            CTRClass ctrClass = new CTRClass();

            using (StreamReader streamReader = new StreamReader("Data/string.txt"))
            {
                text = streamReader.ReadLine();
            }

            var ctrMode = ctrClass.Decrypt(Convert.FromBase64String(text));

            Console.WriteLine($"Text: {Encoding.ASCII.GetString(Convert.FromBase64String(text))}");
            Console.WriteLine($"Use CRT: {Encoding.ASCII.GetString(ctrMode)}\n");

            var test = ctrClass.Encrypt(Encoding.ASCII.GetBytes("Hello world!"));

            Console.WriteLine($"Encoding: {Encoding.ASCII.GetString(test)}");
            Console.WriteLine($"Decoding: {Encoding.ASCII.GetString(ctrClass.Decrypt(test))}");

            Console.ReadKey();
        }
    }
}
