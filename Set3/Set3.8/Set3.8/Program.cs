﻿using System;
using System.Globalization;
using System.IO;
using System.Text;

namespace Set3._8
{
    class Program
    {
        private class MT19937
        {
            private const int U = 11;

            private const int S = 7;

            private const int T = 15;

            private const int L = 18;

            private const uint N = 624;

            private const uint M = 397;

            private const uint A = 0x9908B0DFU; //Последняя строка A[]

            private const uint B = 0x9D2C5680U; //Специально подобранная маска

            private const uint C = 0xEFC60000U; //Специально подобранная маска

            private uint _nextRand;

            private int _stepsToReload = -1;

            private uint[] _state = new uint[N]; //Внутреннее состояние регистра

            private uint HighestBit(uint number)
            {
                return ((number) & 0x80000000U);
            }

            private uint LowestBit(uint number)
            {
                return ((number) & 0x00000001U);
            }

            private uint LowestBits(uint number)
            {
                return ((number) & 0x7FFFFFFFU);
            }

            private uint MixBits(uint numberU, uint numberV)
            {
                return (HighestBit(numberU) | LowestBits(numberV));
            }

            private uint Strongest(uint number) //Закалка
            {
                number ^= (number >> U);
                number ^= (number << S) & B;
                number ^= (number << T) & C;

                return (number ^ (number >> L));
            }

            private uint Reload() //Перемешивание внутреннего состояния регистра
            {
                int j;
                uint s0;
                uint s1;
                uint p0 = 0;
                uint p2 = 2;
                uint pM = M;

                if (_stepsToReload < -1)
                {
                    var help = DateTime.Now.ToString(new CultureInfo("ru-RU")).Split(new char[] { ':', ' ', '.' });
                    Seed(Convert.ToUInt32(help[0] + help[1] + help[3] + help[4] + help[5]));
                }

                for (s0 = _state[0], s1 = _state[1], j = (int)(N - M); Convert.ToBoolean(--j); s0 = s1, s1 = _state[p2++])
                {
                    _state[p0++] = _state[pM++] ^ (MixBits(s0, s1) >> 1) ^ (Convert.ToBoolean(LowestBit(s1)) ? A : 0U);
                }

                for (pM = 0, j = (int)M; Convert.ToBoolean(--j); s0 = s1, s1 = _state[p2++])
                {
                    _state[p0++] = _state[pM++] ^ (MixBits(s0, s1) >> 1) ^ (Convert.ToBoolean(LowestBit(s1)) ? A : 0U);
                }

                _nextRand = 0;
                _stepsToReload = (int)(N - 1);

                s1 = _state[0];
                _state[p0] = _state[pM] ^ (MixBits(s0, s1) >> 1) ^ (Convert.ToBoolean(LowestBit(s1)) ? A : 0U);

                return Strongest(s1);
            }

            public MT19937()
            {
                var help = DateTime.Now.ToString(new CultureInfo("ru-RU")).Split(new char[] { ':', ' ', '.' });
                Seed(Convert.ToUInt32(help[0] + help[1] + help[3] + help[4] + help[5]));
            }

            public MT19937(uint seed)
            {
                Seed(seed);
            }

            public uint StateLength
            {
                get { return N; }

                private set { }
            }

            public uint[] State
            {
                get { return _state; }

                set
                {
                    _stepsToReload = 0;

                    for (int i = 0; i < value.Length; i++)
                    {
                        _state[i] = value[i];
                    }
                }
            }

            public void Seed(uint seed)
            {
                int j;
                int i = 0;
                uint x = seed & 0xFFFFFFFFU;

                for (_stepsToReload = 0, _state[i++] = x, j = (int)N; Convert.ToBoolean(--j); _state[i++] = (x *= 69069U) & 0xFFFFFFFFU) ;
            }

            public uint Random()
            {
                if (--_stepsToReload < 0)
                {
                    return Reload();
                }

                _nextRand = (_nextRand + 1) % N;

                return Strongest(_state[_nextRand]);
            }
        }

        private class CipherMT19937
        {
            private uint _key = 0;

            private MT19937 _encryptorMT19937 = new MT19937();

            private byte[] RepeatingKeyXor(byte[] text, byte[] key)
            {
                byte[] result = new byte[text.Length];

                for (int i = 0; i < text.Length; i++)
                {
                    result[i] = Convert.ToByte(text[i] ^ key[i % key.Length]);
                }

                return result;
            }

            private char GenerateRandomChar(Random random)
            {
                return Convert.ToChar(random.Next(32, 126));
            }

            private byte[] MakePrefix()
            {
                string result = "";
                Random random = new Random();
                int countOfSymbols = random.Next(32) + 1;

                for (int i = 0; i < countOfSymbols; i++)
                {
                    result += GenerateRandomChar(random);
                }

                using (StreamWriter streamWriter = new StreamWriter("Data/prefix.txt"))
                {
                    streamWriter.Write(result);
                }

                return Encoding.ASCII.GetBytes(result);
            }

            private byte[] PrependPrefix(byte[] plaintext)
            {
                int p = 0;
                string str;
                byte[] help;
                byte[] prefix;
                
                using (StreamReader streamReader = new StreamReader("Data/prefix.txt"))
                {
                    prefix = ((str = streamReader.ReadLine()) != null) ? Encoding.ASCII.GetBytes(str) : new byte[] {Convert.ToByte(0)};
                }

                help = new byte[plaintext.Length + prefix.Length];

                for (int i = 0; i < prefix.Length; i++)
                {
                    help[p] = prefix[i];
                    p += 1;
                }

                for (int i = 0; i < plaintext.Length; i++)
                {
                    help[p] = plaintext[i];
                    p += 1;
                }

                return help;
            }

            private byte[] EncryptDecryptAlgorithm(byte[] text)
            {
                if (text == null)
                {
                    return null;
                }
                
                var help = new byte[1];
                var keysStreamLength = 0;
                var bytesOfNumber = new byte[1];
                var keysStream = new byte[1] {Convert.ToByte(0)};

                while (keysStream.Length < text.Length)
                {
                    bytesOfNumber = BitConverter.GetBytes(_encryptorMT19937.Random());
                    keysStreamLength += bytesOfNumber.Length;
                    help = new byte[keysStreamLength];

                    for (int i = 0; i < keysStreamLength - bytesOfNumber.Length; i++)
                    {
                        help[i] = keysStream[i];
                    }

                    for (int i = keysStreamLength - bytesOfNumber.Length; i < keysStreamLength; i++)
                    {
                        help[i] = bytesOfNumber[i - (keysStreamLength - bytesOfNumber.Length)];
                    }

                    keysStream = new byte[keysStreamLength];

                    for (int i = 0; i < keysStreamLength; i++)
                    {
                        keysStream[i] = help[i];
                    }
                }

                if (keysStream.Length > text.Length)
                {
                    help = new byte[text.Length];

                    for (int i = 0; i < text.Length; i++)
                    {
                        help[i] = keysStream[i];
                    }

                    keysStream = new byte[text.Length];

                    for (int i = 0; i < text.Length; i++)
                    {
                        keysStream[i] = help[i];
                    }
                }

                return RepeatingKeyXor(text, keysStream);
            }

            public CipherMT19937(uint seed)
            {
                _key = seed;
                MakePrefix();
            }

            public uint Key
            {
                get { return _key; }

                set { _key = value; }
            }

            public byte[] Encrypt(byte[] plaintext)
            {
                plaintext = PrependPrefix(plaintext);

                _encryptorMT19937.Seed(_key);
                return EncryptDecryptAlgorithm(plaintext);
            }

            public byte[] Decrypt(byte[] ciphertext)
            {
                _encryptorMT19937.Seed(_key);
                return EncryptDecryptAlgorithm(ciphertext);
            }
        }

        private static void RecoverKey()
        {
            Random random = new Random();
            byte[] plaintext = Encoding.ASCII.GetBytes(new string('A', 14));

            var key = random.Next(0, 65535);
            var cipherMT19937 = new CipherMT19937((uint)key);
            var ciphertext = cipherMT19937.Encrypt(plaintext);

            Console.WriteLine("\tRecover key\n");
            Console.WriteLine($"Key: {key}");
            Console.WriteLine($"Ciphertext: {Encoding.ASCII.GetString(ciphertext)}");
            Console.WriteLine($"Plaintext: {Encoding.ASCII.GetString(cipherMT19937.Decrypt(ciphertext))}");

            var answerKey = 0;
            var prefixLength = ciphertext.Length - plaintext.Length;

            for (int i = 0; i < Math.Pow(2, 16); i++)
            {
                var wasMistake = false;
                cipherMT19937.Key = (uint)i;
                var someCipher = cipherMT19937.Encrypt(Encoding.ASCII.GetBytes(new string('A', ciphertext.Length)));

                for (int j = prefixLength; j < ciphertext.Length; j++)
                {
                    if (ciphertext[j] != someCipher[j])
                    {
                        wasMistake = true;
                        break;
                    }
                }

                if (!wasMistake)
                {
                    answerKey = i;
                    break;
                }
            }

            Console.WriteLine();
            Console.WriteLine($"Answer: {answerKey}");
        }

        private static bool IsTokenForCurrentTime(byte[] token, CipherMT19937 cipherMT19937, int time = 0)
        {
            CipherMT19937 cipherMT = cipherMT19937;
            
            if (time == 0)
            {
                cipherMT.Key = (uint) (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            }
            else
            {
                cipherMT.Key = (uint) time;
            }
            
            var plaintext = Encoding.ASCII.GetBytes(new string('A', token.Length));
            var ciphertext = cipherMT.Encrypt(plaintext);

            for (int i = 0; i < token.Length; i++)
            {
                if (ciphertext[i] != token[i])
                {
                    return false;
                }
            }

            return true;
        }

        private static void TokenOracle()
        {
            Random random = new Random();
            Int32 unixTimestamp = (Int32) (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

            var plaintext = Encoding.ASCII.GetBytes(new string('A', random.Next(2, 30)));
            var cipherMT19937 = new CipherMT19937((uint) unixTimestamp);
            var ciphertext = cipherMT19937.Encrypt(plaintext);

            Console.WriteLine("\n\tToken oracle\n");
            Console.WriteLine($"Ciphertext: {Encoding.ASCII.GetString(ciphertext)}\n");
            Console.WriteLine($"Is token for current time (1): {IsTokenForCurrentTime(ciphertext, cipherMT19937, unixTimestamp)}");
            Console.WriteLine($"Is token for current time (2): {IsTokenForCurrentTime(ciphertext, cipherMT19937, unixTimestamp + 2)}");
        }

        static void Main(string[] args)
        {
            RecoverKey();
            TokenOracle();

            Console.ReadKey();
        }
    }
}
