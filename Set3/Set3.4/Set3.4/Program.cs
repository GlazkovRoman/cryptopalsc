﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Set3._4
{
    internal class Program
    {
        private class CTRClass
        {
            private AesManaged _aesAlg = new AesManaged();

            private EncryptionDelegate Encryption = null;

            private ChangingCounterDelegate ChangingCounter = null;

            public delegate byte[] EncryptionDelegate(string plaintext);

            public delegate char[] ChangingCounterDelegate(char[] counter, int value);

            private byte[] RepeatingKeyXor(byte[] text, byte[] key)
            {
                byte[] result = new byte[text.Length];

                for (int i = 0; i < text.Length; i++)
                {
                    result[i] = Convert.ToByte(text[i] ^ key[i%key.Length]);
                }

                return result;
            }

            private void InitAes()
            {
                _aesAlg.KeySize = 128;
                _aesAlg.BlockSize = 128;

                _aesAlg.GenerateKey();

                _aesAlg.Mode = CipherMode.ECB;
                _aesAlg.Padding = PaddingMode.None;
            }

            private byte[] EncryptStringToBytesAes(string plaintext)
            {
                if (plaintext == null || plaintext.Length <= 0)
                    throw new ArgumentNullException("plaintext");

                byte[] encrypted;

                ICryptoTransform encryptor = _aesAlg.CreateEncryptor(_aesAlg.Key, _aesAlg.IV);

                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(plaintext);
                        }

                        encrypted = msEncrypt.ToArray();
                    }
                }

                return encrypted;
            }

            private char[] CounterLastBlockCount(char[] counter, int value)
            {
                counter[8] = Convert.ToChar((counter[8] + value)%256);
                return counter;
            }

            public CTRClass()
            {
                InitAes();

                Encryption = EncryptStringToBytesAes;
                ChangingCounter = CounterLastBlockCount;
            }

            public CTRClass(ChangingCounterDelegate changingCounterDelegate, EncryptionDelegate encryptionDelegate)
            {
                InitAes();

                Encryption = encryptionDelegate;
                ChangingCounter = changingCounterDelegate;
            }

            public byte[] Encrypt(byte[] text, int blockSize = 16)
            {
                int start = 0;
                int finish = 0;

                byte[] help = new byte[blockSize];
                byte[] result = new byte[text.Length];

                char[] counter = new string(Convert.ToChar(0), blockSize).ToCharArray();
                byte[] counterCipher = Encryption(new string(counter));

                for (int i = 0; i < Math.Ceiling((double) text.Length/(double) blockSize); i++)
                {
                    if (i*blockSize + blockSize <= text.Length)
                    {
                        start = i*blockSize;
                        finish = i*blockSize + blockSize;
                    }
                    else
                    {
                        start = i*blockSize;
                        finish = text.Length;
                    }

                    help = new byte[finish - start];

                    for (int j = start; j < finish; j++)
                    {
                        help[j - i*blockSize] = text[j];
                    }

                    byte[] addToResult = RepeatingKeyXor(help, counterCipher);

                    for (int j = start; j < finish; j++)
                    {
                        result[j] = addToResult[j - i*blockSize];
                    }
                }

                return result;
            }

            public byte[] Decrypt(byte[] text, int blockSize = 16)
            {
                return Encrypt(text, blockSize);
            }
        }

        private class BrutforceAndFrequencyMethodClass
        {
            private byte[][] _ciphersArray;

            private byte[] RepeatingKeyXor(byte[] text, byte[] key)
            {
                byte[] result = new byte[text.Length];

                for (int i = 0; i < text.Length; i++)
                {
                    result[i] = Convert.ToByte(text[i] ^ key[i%key.Length]);
                }

                return result;
            }

            private byte[][] FindKeysMeanings(int blockSize = 16)
            {
                int xor = 0;
                int count = 0;

                bool wasBroken = false;

                byte[] help = new byte[1];
                byte[] controlSample = new byte[1];
                byte[][] keysMeanings = new byte[blockSize][];

                for (int i = 0; i < blockSize; i++)
                {
                    keysMeanings[i] = new byte[1];
                }

                for (int keyCharNumber = 0; keyCharNumber < blockSize; keyCharNumber++)
                {
                    count = 0;
                    controlSample = new byte[1];

                    for (int i = 0; i < _ciphersArray.Length; i++)
                    {
                        for (int j = keyCharNumber; j < _ciphersArray[i].Length; j += blockSize)
                        {
                            count += 1;
                            help = new byte[count];

                            for (int k = 0; k < count - 1; k++)
                            {
                                help[k] = controlSample[k];
                            }

                            help[count - 1] = _ciphersArray[i][j];
                            controlSample = help;
                        }
                    }

                    count = 0;
                    help = new byte[1];

                    for (int c = 0; c < 256; c++)
                    {
                        wasBroken = false;

                        for (int i = 0; i < controlSample.Length; i++)
                        {
                            xor = c ^ controlSample[i];

                            if ((xor < 44 || xor > 122) && xor != 10 && xor != 13 && xor != 32 && xor != 33 && xor != 34 && xor != 39)
                            {
                                wasBroken = true;
                                break;
                            }
                        }

                        if (!wasBroken)
                        {
                            count += 1;
                            help = new byte[count];

                            for (int i = 0; i < count - 1; i++)
                            {
                                help[i] = keysMeanings[keyCharNumber][i];
                            }

                            help[count - 1] = Convert.ToByte(c);
                            keysMeanings[keyCharNumber] = new byte[count];

                            for (int i = 0; i < count; i++)
                            {
                                keysMeanings[keyCharNumber][i] = help[i];
                            }
                        }
                    }
                }

                return keysMeanings;
            }

            private Dictionary<int, int> MakeDictionary(byte[][] plaintext)
            {
                Dictionary<int, int> result = new Dictionary<int, int>();
                Dictionary<int, int> dictionary = new Dictionary<int, int>();

                for (int i = 0; i < plaintext.Length; i++)
                {
                    for (int j = 0; j < plaintext[i].Length; j++)
                    {
                        int key = plaintext[i][j];

                        if (dictionary.ContainsKey(key))
                        {
                            dictionary[key] += 1;
                        }
                        else
                        {
                            dictionary.Add(key, 1);
                        }
                    }
                }

                foreach (KeyValuePair<int, int> dict in dictionary.OrderByDescending(key => key.Value))
                {
                    result.Add(dict.Key, dict.Value);
                }

                return result;
            }

            private bool FrequencyMethod(byte[][] plaintext, string topFrequencyCharacters = " etaoinshr")
            {
                var dictionary = MakeDictionary(plaintext);

                for (int i = 0; i < Math.Min(topFrequencyCharacters.Length, dictionary.Count); i++)
                {
                    if (topFrequencyCharacters.IndexOf(Convert.ToChar(dictionary.ElementAt(i).Key)) == -1)
                    {
                        return false;
                    }
                }

                return true;
            }

            private bool TrigramsMethod(byte[][] plaintext, string trigrams = "the, and, ing, her, tha, ere, " +
                                                                              "hat, eth, ent, nth, for, his, " +
                                                                              "thi, ter, int, dth, you, all, " +
                                                                              "hes, ion, ith, oth, est, tth, " +
                                                                              "oft, ver, sth, ers, fth, rea, oid")
            {
                var trigramsSplit = trigrams.Split(new char[] {' ', ','}, StringSplitOptions.RemoveEmptyEntries);

                for (int i = 0; i < plaintext.Length; i++)
                {
                    for (int j = 0; j < trigramsSplit.Length; j++)
                    {
                        if (
                            Encoding.ASCII.GetString(plaintext[i]).IndexOf(trigramsSplit[j], StringComparison.Ordinal) !=
                            -1)
                        {
                            return true;
                        }
                    }
                }

                return false;
            }

            private bool BigramsMethod(byte[][] plaintext, string bigrams = "th, he, in, er, an, re, es, nd, " +
                                                                            "st, on, en, ea, at, ed, nt, ha, " +
                                                                            "to, or, ou, ng, et, it, ar, te, " +
                                                                            "is, ti, hi, as, of, se, ed")
            {
                var bigramsSplit = bigrams.Split(new char[] {' ', ','}, StringSplitOptions.RemoveEmptyEntries);

                for (int i = 0; i < plaintext.Length; i++)
                {
                    for (int j = 0; j < bigramsSplit.Length; j++)
                    {
                        if (Encoding.ASCII.GetString(plaintext[i]).IndexOf(bigramsSplit[j], StringComparison.Ordinal) !=
                            -1)
                        {
                            return true;
                        }
                    }
                }

                return false;
            }

            private void BrutforceKey(byte[][] ciphersArray, byte[][] keysMeanings, byte[] currentKey, 
                StreamWriter streamWriter, int blockSize = 16, int currentKeySymbol = 0)
            {
                if (currentKeySymbol < blockSize)
                {
                    for (int i = 0; i < keysMeanings[currentKeySymbol].Length; i++)
                    {
                        currentKey[currentKeySymbol] = keysMeanings[currentKeySymbol][i];
                        BrutforceKey(ciphersArray, keysMeanings, currentKey, streamWriter, blockSize, currentKeySymbol + 1);
                    }
                }
                else
                {
                    byte[][] plaintext = new byte[ciphersArray.Length][];

                    for (int i = 0; i < ciphersArray.Length; i++)
                    {
                        plaintext[i] = RepeatingKeyXor(ciphersArray[i], currentKey);
                    }

                    if ((BigramsMethod(plaintext) && TrigramsMethod(plaintext)) || FrequencyMethod(plaintext))
                    {
                        string text = "";
                        string plaintextValue = "";

                        for (int i = 0; i < plaintext.Length; i++)
                        {
                            text = "";

                            for (int j = 0; j < plaintext[i].Length; j++)
                            {
                                if (j % blockSize == 0 && j != 0 && j != plaintext[i].Length - 1)
                                {
                                    text += " :: ";
                                }

                                text += Convert.ToChar(plaintext[i][j]);
                            }

                            plaintextValue += $"({i + 1}) : {text}\n";
                        }

                        streamWriter.WriteLine($"Key: {Encoding.ASCII.GetString(currentKey)}\n\nText:\n\n{plaintextValue}");
                    }
                }
            }

            public BrutforceAndFrequencyMethodClass(byte[][] ciphersArray)
            {
                _ciphersArray = ciphersArray;
            }

            public byte[][] CiphersArray
            {
                get { return _ciphersArray; }

                set { _ciphersArray = value; }
            }

            public void BrutforceAndFrequencyMethod(int blockSize = 16, List<int> sizeOfParts = null)
            {
                byte[] currentKey = null;
                byte[][] keysMeanings = FindKeysMeanings(blockSize);
                
                if (sizeOfParts == null)
                {
                    currentKey = new byte[blockSize];

                    for (int i = 0; i < blockSize; i++)
                    {
                        currentKey[i] = Convert.ToByte(0);
                    }

                    using (
                        StreamWriter streamWriter =
                            new StreamWriter("Data/bruteforceResult/bruteforceResult.txt"))
                    {
                        BrutforceKey(_ciphersArray, keysMeanings, currentKey, streamWriter, blockSize);
                    }
                }
                else
                {
                    int p = 0;
                    int count = 0;
                    int start = 0;
                    int finish = 0;

                    foreach (var size in sizeOfParts)
                    {
                        byte[] help = new byte[1];
                        byte[][] partOfKeysMeanings = new byte[size][];
                        byte[][] ciphersArray = new byte[_ciphersArray.Length][];

                        if (start >= keysMeanings.Length)
                        {
                            return;
                        }

                        p = 0;
                        finish = start + size;
                        finish = Math.Min(finish, keysMeanings.Length);

                        for (int i = start; i < finish; i++)
                        {
                            partOfKeysMeanings[p] = new byte[keysMeanings[i].Length];

                            for (int j = 0; j < keysMeanings[i].Length; j++)
                            {
                                partOfKeysMeanings[p][j] = keysMeanings[i][j];
                            }

                            p += 1;
                        }

                        currentKey = new byte[size];

                        for (int i = 0; i < size; i++)
                        {
                            currentKey[i] = Convert.ToByte(0);
                        }

                        for (int i = 0; i < _ciphersArray.Length; i++)
                        {
                            count = 0;

                            for (int j = 0; j < _ciphersArray[i].Length; j += blockSize)
                            {
                                for (int k = j + start; k < Math.Min(j + finish, _ciphersArray[i].Length); k++)
                                {
                                    help = new byte[count + 1];

                                    for (int c = 0; c < count; c++)
                                    {
                                        help[c] = ciphersArray[i][c];
                                    }

                                    help[count] = _ciphersArray[i][k];
                                    ciphersArray[i] = new byte[count + 1];

                                    for (int c = 0; c < count + 1; c++)
                                    {
                                        ciphersArray[i][c] = help[c];
                                    }

                                    count += 1;
                                }
                            }
                        }

                        using (
                            StreamWriter streamWriter =
                                new StreamWriter($"Data/bruteforceResult/bruteforceResult({start.ToString()} - {finish.ToString()}).txt"))
                        {
                            BrutforceKey(ciphersArray, partOfKeysMeanings, currentKey, streamWriter, size);
                        }
                        
                        start += size;
                    }
                }
            }
        }

        private static void Main(string[] args)
        {
            int smallestLength = int.MaxValue;
            Stopwatch stopwatch = new Stopwatch();

            CTRClass ctrClass = new CTRClass();
            List<string> strList = new List<string>();

            using (StreamReader streamReader = new StreamReader("Data/base64Strings.txt"))
            {
                string help = "";

                while ((help = streamReader.ReadLine()) != null)
                {
                    strList.Add(Encoding.ASCII.GetString(Convert.FromBase64String(help)));

                    //Console.WriteLine(strList[strList.Count - 1]);
                    //streamWriter.WriteLine($"{strList[strList.Count - 1]}\n");
                }
            }

            byte[][] ciphersArray = new byte[strList.Count][];
            //Console.WriteLine();

            for (int i = 0; i < strList.Count; i++)
            {
                ciphersArray[i] = ctrClass.Encrypt(Encoding.ASCII.GetBytes(strList[i]));

                if (smallestLength > ciphersArray[i].Length)
                {
                    smallestLength = ciphersArray[i].Length;
                }

                //Console.WriteLine(Encoding.ASCII.GetString(ciphersArray[i]));
            }

            byte[][] ciphersArrayForAlg = new byte[strList.Count][];

            for (int i = 0; i < strList.Count; i++)
            {
                ciphersArrayForAlg[i] = new byte[smallestLength];

                for (int j = 0; j < smallestLength; j++)
                {
                    ciphersArrayForAlg[i][j] = ciphersArray[i][j];
                }
            }

            stopwatch.Start();

            List<int> sizeOfParts = new List<int>() {18, 18, smallestLength - 36};

            var brutforceAndFrequencyMethodClass = new BrutforceAndFrequencyMethodClass(ciphersArrayForAlg);
            brutforceAndFrequencyMethodClass.BrutforceAndFrequencyMethod(smallestLength, sizeOfParts);

            stopwatch.Stop();

            var ts = stopwatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds/10);
            
            Console.WriteLine("RunTime: " + elapsedTime);
            Console.ReadKey();

            /*var test = ctrClass.Encrypt(Encoding.ASCII.GetBytes("Hello world!"));

            Console.WriteLine();
            Console.WriteLine($"Encoding: {Encoding.ASCII.GetString(test)}");
            Console.WriteLine($"Decoding: {Encoding.ASCII.GetString(ctrClass.Decrypt(test))}");

            Console.WriteLine("Program has finished.");
            Console.ReadKey();*/
        }
    }
}
