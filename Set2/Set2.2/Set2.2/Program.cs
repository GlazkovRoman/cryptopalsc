﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Set2._2
{
    class Program
    {
        static byte[] EncryptStringToBytesAes(string plainText, byte[] key, byte[] IV)
        {
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (key == null || key.Length <= 0)
                throw new ArgumentNullException("key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("key");

            byte[] encrypted;

            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.IV = IV;
                aesAlg.Key = key;
                aesAlg.Mode = CipherMode.CBC;
                aesAlg.Padding = PaddingMode.PKCS7;

                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(plainText);
                        }

                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            return encrypted;
        }

        private static string DecryptStringFromBytesAes(byte[] cipherText, byte[] key, byte[] IV)
        {
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (key == null || key.Length <= 0)
                throw new ArgumentNullException("key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            string plaintext = null;

            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.IV = IV;
                aesAlg.Key = key;
                aesAlg.Mode = CipherMode.CBC;
                aesAlg.Padding = PaddingMode.PKCS7;

                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }

            return plaintext;
        }

        static private void TestDecryptor()
        {
            string cipher = null;

            using (StreamReader streamReader = new StreamReader("Data/cipher.txt"))
            {
                string help = null;

                while ((help = streamReader.ReadLine()) != null)
                {
                    cipher += help;
                }
            }

            using (StreamWriter streamWriter = new StreamWriter("Data/plaintext.txt"))
            {
                string plaintext = DecryptStringFromBytesAes(Convert.FromBase64String(cipher), Encoding.ASCII.GetBytes("YELLOW SUBMARINE"),
                    Encoding.ASCII.GetBytes("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"));

                streamWriter.WriteLine(plaintext);

                Console.WriteLine(plaintext);
                Console.ReadKey();
            }
        }

        static private void TestEncryptor()
        {
            string plaintext = null;
            string cipherTest = null;

            using (StreamReader streamReader = new StreamReader("Data/plaintext.txt"))
            {
                plaintext = streamReader.ReadToEnd();
                plaintext = plaintext.Substring(0, plaintext.Length - 2);
            }
            
            using (StreamReader streamReader = new StreamReader("Data/cipher.txt"))
            {
                string help = null;

                while ((help = streamReader.ReadLine()) != null)
                {
                    cipherTest += help;
                }
            }

            using (StreamWriter streamWriter = new StreamWriter("Data/cipherTest.txt"))
            {
                var cipher = EncryptStringToBytesAes(plaintext, Encoding.ASCII.GetBytes("YELLOW SUBMARINE"),
                    Encoding.ASCII.GetBytes("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"));
                var cipherTestBytes = Convert.FromBase64String(cipherTest);

                for (int i = 0; i < cipherTestBytes.Length; i++)
                {
                    System.Diagnostics.Debug.Assert(cipher[i] == cipherTestBytes[i], $"Wrong: {i}");
                }
                
                streamWriter.WriteLine("Program has been finished.");

                Console.WriteLine("Program has been finished.");
                Console.ReadKey();
            }
        }

        static void Main(string[] args)
        {
            //TestEncryptor();
            TestDecryptor();
        }
    }
}
