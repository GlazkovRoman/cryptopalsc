﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Set2._3
{
    class Program
    {
        private static char GenerateRandomChar()
        {
            Random random = new Random();
            return Convert.ToChar(random.Next(32, 126));
        }

        private static string AppendToPlaintext(string plaintext)
        {
            string result = "";
            Random random = new Random();

            int countOfSymbolsEnd = random.Next(5) + 5;
            int countOfSymbolsStart = random.Next(5) + 5;

            for (int i = 0; i < countOfSymbolsStart; i++)
            {
                result += GenerateRandomChar();
            }

            result += plaintext;

            for (int i = 0; i < countOfSymbolsEnd; i++)
            {
                result += GenerateRandomChar();
            }

            return result;
        }

        private static byte[] EncryptStringToBytesAes(string plainText)
        {
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");

            byte[] encrypted;

            using (AesManaged aesAlg = new AesManaged())
            {
                Random random = new Random();

                aesAlg.GenerateIV();
                aesAlg.GenerateKey();
                aesAlg.Padding = PaddingMode.PKCS7;

                if (random.Next() % 2 == 0)
                {
                    aesAlg.Mode = CipherMode.ECB;
                }
                else
                {
                    aesAlg.Mode = CipherMode.CBC;
                }
                
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            plainText = AppendToPlaintext(plainText);
                            swEncrypt.Write(plainText);
                        }

                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            return encrypted;
        }

        private static bool DetectEcbMode(string cipher)
        {
            int step = 16;
            string help1 = null;
            string help2 = null;

            for (int i = 0; i < cipher.Length; i += step)
            {
                if (cipher.Length - i > step)
                {
                    help1 = cipher.Substring(i, step);
                    help2 = cipher.Substring(i + step);

                    if (help2.IndexOf(help1) != -1)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        static void Main(string[] args)
        {
            string plaintext = "";

            using (StreamReader streamReader = new StreamReader("Data/plaintext.txt"))
            {
                plaintext = streamReader.ReadLine();
            }

            string cipher = Encoding.ASCII.GetString(EncryptStringToBytesAes(plaintext));

            if (DetectEcbMode(cipher))
            {
                Console.WriteLine($"Text was encrypted in ECB mode.\n\nText: {cipher}");
            }
            else
            {
                Console.WriteLine($"Text was encrypted in CBC mode.\n\nText: {cipher}");
            }
            
            Console.ReadKey();
        }
    }
}
