﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Set2._6
{
    class Program
    {
        private static AesManaged _aesAlg = new AesManaged();

        private static char GenerateRandomChar(Random random)
        {
            return Convert.ToChar(random.Next(32, 126));
        }

        private static string MakePrefix()
        {
            string result = "";
            Random random = new Random();
            int countOfSymbols = random.Next(32) + 1;

            for (int i = 0; i < countOfSymbols; i++)
            {
                result += GenerateRandomChar(random);
            }

            using (StreamWriter streamWriter = new StreamWriter("Data/prefix.txt"))
            {
                streamWriter.Write(result);
            }

            return result;
        }

        private static string PrependPrefix(string str)
        {
            string help = "";

            using (StreamReader streamReader = new StreamReader("Data/prefix.txt"))
            {
                help = streamReader.ReadLine();
            }

            return help += str;
        }

        private static string AppendToString(string str)
        {
            string base64Text = "";

            using (StreamReader streamReader = new StreamReader("Data/append.txt"))
            {
                base64Text = streamReader.ReadLine();
            }

            return str + Encoding.ASCII.GetString(Convert.FromBase64String(base64Text));
        }

        private static byte[] EncryptStringToBytesAes(string plainText)
        {
            byte[] encrypted;

            Random random = new Random();
            
            _aesAlg.GenerateIV();
            _aesAlg.Mode = CipherMode.ECB;
            _aesAlg.Padding = PaddingMode.PKCS7;

            ICryptoTransform encryptor = _aesAlg.CreateEncryptor(_aesAlg.Key, _aesAlg.IV);

            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                    {
                        plainText = AppendToString(plainText);
                        plainText = PrependPrefix(plainText);
                        swEncrypt.Write(plainText);
                    }

                    encrypted = msEncrypt.ToArray();
                }
            }

            return encrypted;
        }

        private static bool HasEqualBlocks(string cipher)
        {
            int step = 16;
            string help1 = null;
            string help2 = null;

            for (int i = 0; i < cipher.Length; i += step)
            {
                if (cipher.Length - i > step)
                {
                    help1 = cipher.Substring(i, step);
                    help2 = cipher.Substring(i + step);

                    if (help2.IndexOf(help1) != -1)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private static int FindEndBlockOfPrefix(int blockSize = 16)
        {
            var cipher1 = EncryptStringToBytesAes("");
            var cipher2 = EncryptStringToBytesAes("A");

            for (int i = 0; i < cipher1.Length; i++)
            {
                if (cipher1[i] != cipher2[i])
                {
                    return (int)Math.Floor((double)i / (double)blockSize);
                }
            }

            return 0;
        }

        private static int FindEndPartOfPrefix(int blockSize = 16)
        {
            for (int i = 0; i < blockSize; i++)
            {
                var cipher = EncryptStringToBytesAes(new string('A', 2 * blockSize + i));

                if (HasEqualBlocks(Encoding.ASCII.GetString(cipher)))
                {
                    return (blockSize - i) % blockSize;
                }
            }

            return 0;
        }

        private static string FindPlaintext(string ciphertext, int blockSize = 16, string chars = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm1234567890,.~`:;?!-\n /'")
        {
            int blockNumber = 0;

            string help = "";
            string result = "";
            string brutBlock = "";
            string cipherBlock = "";
            string identifierStr = "";

            int prefixLength = FindEndBlockOfPrefix() * blockSize + FindEndPartOfPrefix();

            for (int i = 0; i < ciphertext.Length - prefixLength; i++)
            {
                identifierStr = "";

                for (int j = 0; j < (blockSize - 1) - (i % blockSize) + ((blockSize - prefixLength % blockSize) % blockSize); j++)
                {
                    identifierStr += 'A';
                }
                
                brutBlock = identifierStr + result + "A";
                blockNumber = (int)Math.Floor(((double)prefixLength + (double)brutBlock.Length - 1) / (double)blockSize);
                cipherBlock = Encoding.ASCII.GetString(EncryptStringToBytesAes(identifierStr)).Substring(blockNumber * blockSize, blockSize);

                for (int j = 0; j < chars.Length; j++)
                {
                    brutBlock = brutBlock.Substring(0, brutBlock.Length - 1);
                    brutBlock += chars[j];
                    help = Encoding.ASCII.GetString(EncryptStringToBytesAes(brutBlock)).Substring(blockNumber * blockSize, blockSize);

                    if (help == cipherBlock)
                    {
                        result += chars[j];
                        break;
                    }
                }
            }

            return result;
        }

        static void Main(string[] args)
        {
            string prefix = MakePrefix();
            string append = AppendToString("");
            
            _aesAlg.KeySize = 128;
            _aesAlg.BlockSize = 128;
            _aesAlg.GenerateKey(); //Генерируем ключ

            var ciphertext = Encoding.ASCII.GetString(EncryptStringToBytesAes(""));
            
            Console.WriteLine($"Text: {append}");
            Console.WriteLine($"Prefix: {prefix}\n");
            Console.WriteLine($"Answer: {FindPlaintext(ciphertext)}");

            Console.ReadKey();
        }
    }
}
