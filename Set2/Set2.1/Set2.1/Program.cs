﻿using System;

namespace Set2._1
{
    class Program
    {
        private static string PaddingPKCS7(string message, int blockSize)
        {
            int padding = 0;
            string result = message;

            if (message.Length % blockSize != 0)
            {
                padding = blockSize - (message.Length % blockSize);

                for (int i = 0; i < padding; i++)
                {
                    result += Convert.ToChar(padding);
                }
            }
            
            return result;
        }

        static void Main(string[] args)
        {
            int blockSize = 0;
            string str = null;
            string answer = null;

            Console.Write("Enter a block size: ");
            blockSize = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter a string: ");
            str = Console.ReadLine();

            answer = PaddingPKCS7(str, blockSize);

            Console.WriteLine("\nAnswer: " + answer);
            Console.ReadKey();
        }
    }
}
