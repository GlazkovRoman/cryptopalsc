﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Set2._4
{
    class Program
    {
        private static AesManaged _aesAlg = new AesManaged();

        private static string AppendToString(string str)
        {
            string base64Text = "";

            using (StreamReader streamReader = new StreamReader("Data/append.txt"))
            {
                base64Text = streamReader.ReadLine();
            }

            return str + Encoding.ASCII.GetString(Convert.FromBase64String(base64Text));
        }

        private static byte[] EncryptStringToBytesAes(string plainText)
        {
            byte[] encrypted;

            Random random = new Random();

            _aesAlg.GenerateIV();
            _aesAlg.Mode = CipherMode.ECB;
            _aesAlg.Padding = PaddingMode.PKCS7;
            
            ICryptoTransform encryptor = _aesAlg.CreateEncryptor(_aesAlg.Key, _aesAlg.IV);

            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                    {
                        plainText = AppendToString(plainText);
                        swEncrypt.Write(plainText);
                    }

                    encrypted = msEncrypt.ToArray();
                }
            }

            return encrypted;
        }

        private static string FindPlaintext(string ciphertext, int blockSize = 16, string chars = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm1234567890,.~`:;?!-\n /'")
        {
            string help = "";
            string result = "";
            string brutBlock = "";
            string cipherBlock = "";
            string identifierStr = "";

            for (int i = 0; i < ciphertext.Length; i++)
            {
                identifierStr = "";

                for (int j = 0; j < (blockSize - 1) - (i % blockSize); j++)
                {
                    identifierStr += 'A';
                }

                brutBlock = identifierStr + result + "A";
                cipherBlock = Encoding.ASCII.GetString(EncryptStringToBytesAes(identifierStr)).Substring((int)Math.Floor((double)i / (double)blockSize) * blockSize, blockSize);
                
                for (int j = 0; j < chars.Length; j++)
                {
                    brutBlock = brutBlock.Substring(0, brutBlock.Length - 1);
                    brutBlock += chars[j];
                    help = Encoding.ASCII.GetString(EncryptStringToBytesAes(brutBlock)).Substring((int)Math.Floor((double)i / (double)blockSize) * blockSize, blockSize);

                    if (help == cipherBlock)
                    {
                        result += chars[j];
                        break;
                    }
                }
            }

            return result;
        }

        static void Main(string[] args)
        {
            string append = AppendToString("");
            string ciphertext = Encoding.ASCII.GetString(EncryptStringToBytesAes(""));

            _aesAlg.KeySize = 128;
            _aesAlg.GenerateKey(); //Генерируем ключ
            
            Console.WriteLine($"Text: {append}");
            Console.WriteLine($"Answer: {FindPlaintext(ciphertext)}");
            Console.ReadKey();
        }
    }
}
