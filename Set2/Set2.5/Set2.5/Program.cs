﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using Newtonsoft.Json.Linq;

namespace Set2._5
{
    class Program
    {
        private static AesManaged _aesAlg = new AesManaged();

        static byte[] EncryptStringToBytesAes(string plainText)
        {
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");

            byte[] encrypted;
            
            _aesAlg.Mode = CipherMode.ECB;
            _aesAlg.Padding = PaddingMode.PKCS7;

            ICryptoTransform encryptor = _aesAlg.CreateEncryptor(_aesAlg.Key, _aesAlg.IV);

            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                    {
                        swEncrypt.Write(plainText);
                    }

                    encrypted = msEncrypt.ToArray();
                }
            }

            return encrypted;
        }

        private static string DecryptStringFromBytesAes(byte[] cipherText)
        {
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");

            string plaintext = null;

            _aesAlg.Mode = CipherMode.ECB;
            _aesAlg.Padding = PaddingMode.PKCS7;

            ICryptoTransform decryptor = _aesAlg.CreateDecryptor(_aesAlg.Key, _aesAlg.IV);

            using (MemoryStream msDecrypt = new MemoryStream(cipherText))
            {
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                {
                    using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                    {
                        plaintext = srDecrypt.ReadToEnd();
                    }
                }
            }

            return plaintext;
        }

        private static dynamic ParsingRoutine(string str)
        {
            dynamic result;
            string jsonStr = "{";

            var help = str.Split(new char[] {'=', '&'}, StringSplitOptions.RemoveEmptyEntries);

            if (help.Length % 2 != 0)
            {
                return null;
            }

            for (int i = 0; i < help.Length; i += 2)
            {
                jsonStr += $"\"{help[i]}\":\"{help[i + 1]}\",";
            }

            jsonStr = jsonStr.Substring(0, jsonStr.Length - 1);

            jsonStr += "}";
            result = JObject.Parse(jsonStr);

            return result;
        }

        private static KeyValuePair<dynamic, string> Profile(string mail)
        {
            dynamic resultKey;
            string resultValue = "";

            if (mail.IndexOf("&") != -1 || mail.IndexOf("=") != -1)
            {
                return new KeyValuePair<dynamic, string>(null, "");
            }

            resultValue += $"email={mail}&uid=10&role=user";
            resultKey = ParsingRoutine(resultValue);

            return new KeyValuePair<dynamic, string>(resultKey, resultValue);
        }

        private static string PaddingPKCS7(string message, int blockSize)
        {
            int padding = 0;
            string result = message;

            if (message.Length % blockSize != 0)
            {
                padding = blockSize - (message.Length % blockSize);

                for (int i = 0; i < padding; i++)
                {
                    result += Convert.ToChar(padding);
                }
            }

            return result;
        }

        private static byte[] EncryptedProfile(string mail)
        {
            return EncryptStringToBytesAes(Profile(mail).Value);
        }

        private static dynamic DecryptedProfile(byte[] cipherText)
        {
            return ParsingRoutine(DecryptStringFromBytesAes(cipherText));
        }

        private static string MakeRole(string role, int blockSize)
        {
            string fakeMail = new string('A', 10);
            string result = fakeMail + PaddingPKCS7(role, blockSize);
            
            return result;
        }

        private static void TestMakeRole()
        {
            int blockSize = 16;

            var ch1 = EncryptedProfile("u@trustme.com");
            var ch2 = EncryptedProfile(MakeRole("admin", blockSize));

            for (int i = 0; i < blockSize; i++)
            {
                ch1[blockSize * 2 + i] = ch2[blockSize + i];
            }

            Console.WriteLine(DecryptedProfile(ch1));
        }

        static void Main(string[] args)
        {
            /*string message = "foo=bar&baz=qux&zap=zazzle";
            
            _aesAlg.GenerateKey();

            var profile = Profile("glazkor95@mail.ru");
            var cipher = EncryptStringToBytesAes(profile.Value);
            var plaintext = DecryptStringFromBytesAes(cipher);

            Console.WriteLine($"ParsingRoutine:\n{ParsingRoutine(message)}\n");
            Console.WriteLine($"Profile:\n{profile}\n");
            Console.WriteLine($"Encrypt: {Encoding.ASCII.GetString(cipher)}");
            Console.WriteLine($"Decrypt: {plaintext}");
            Console.WriteLine($"\nDecrypt and parce:\n{ParsingRoutine(plaintext)}");*/

            TestMakeRole();

            Console.ReadKey();
        }
    }
}
