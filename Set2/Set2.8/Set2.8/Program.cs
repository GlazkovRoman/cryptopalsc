﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Set2._8
{
    class Program
    {
        private static AesManaged _aesAlg = new AesManaged();

        private static string PrependText(string str)
        {
            string prependText = "";

            using (StreamReader streamReader = new StreamReader("Data/prepend.txt"))
            {
                prependText = streamReader.ReadLine();
            }

            return prependText + str;
        }

        private static string AppendText(string str)
        {
            string appendText = "";

            using (StreamReader streamReader = new StreamReader("Data/append.txt"))
            {
                appendText = streamReader.ReadLine();
            }

            return str + appendText;
        }

        private static string ReplaceSomeChars(string str, string chars = ";=")
        {
            string result = str;

            for (int i = 0; i < chars.Length; i++)
            {
                result = result.Replace(chars[i], '_');
            }

            return result;
        }

        static byte[] EncryptStringToBytesAes(string plaintext)
        {
            if (plaintext == null || plaintext.Length <= 0)
                throw new ArgumentNullException("plaintext");

            byte[] encrypted;
            
            _aesAlg.Mode = CipherMode.CBC;
            _aesAlg.Padding = PaddingMode.PKCS7;

            ICryptoTransform encryptor = _aesAlg.CreateEncryptor(_aesAlg.Key, _aesAlg.IV);

            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                    {
                        plaintext = ReplaceSomeChars(plaintext);

                        plaintext = PrependText(plaintext);
                        plaintext = AppendText(plaintext);

                        swEncrypt.Write(plaintext);
                    }

                    encrypted = msEncrypt.ToArray();
                }
            }

            return encrypted;
        }

        private static string DecryptStringFromBytesAes(byte[] cipherText)
        {
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");

            string plaintext = null;

            _aesAlg.Mode = CipherMode.CBC;
            _aesAlg.Padding = PaddingMode.PKCS7;

            ICryptoTransform decryptor = _aesAlg.CreateDecryptor(_aesAlg.Key, _aesAlg.IV);

            using (MemoryStream msDecrypt = new MemoryStream(cipherText))
            {
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                {
                    using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                    {
                        plaintext = srDecrypt.ReadToEnd();
                    }
                }
            }

            return plaintext;
        }

        private static bool DecryptAndFind(byte[] cipherText)
        {
            var cipher = cipherText;

            cipher[22] ^= Convert.ToByte(Convert.ToByte('_') ^ Convert.ToByte('='));
            cipher[16] ^= Convert.ToByte(Convert.ToByte('_') ^ Convert.ToByte(';'));

            var plaintext = DecryptStringFromBytesAes(cipher);
            Console.WriteLine($"Plaintext: {plaintext}\n");

            if (plaintext.IndexOf("admin=true") != -1)
            {
                return true;
            }

            return false;
        }

        static void Main(string[] args)
        {
            string text = ";admin=true";

            _aesAlg.GenerateIV();
            _aesAlg.GenerateKey();

            var cipher = EncryptStringToBytesAes(text);

            Console.WriteLine($"Cipher: {Encoding.ASCII.GetString(cipher)}\n");
            Console.WriteLine($"Answer: {DecryptAndFind(cipher)}");

            Console.ReadKey();
        }
    }
}
