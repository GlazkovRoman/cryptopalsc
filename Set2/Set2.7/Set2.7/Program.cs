﻿using System;

namespace Set2._7
{
    class Program
    {
        private static string PaddingPKCS7(string message, int blockSize = 16)
        {
            int padding = 0;
            string result = message;

            padding = blockSize - (message.Length % blockSize);

            for (int i = 0; i < padding; i++)
            {
                result += Convert.ToChar(padding);
            }

            return result;
        }

        private static string PaddingValidation(string text)
        {
            int padding = 0;
            string result = text;

            padding = result[result.Length - 1];

            for (int i = result.Length - 1; i > (result.Length - 1) - padding; i--)
            {
                if (result[i] != Convert.ToChar(padding))
                {
                    return "";
                }
            }

            return result.Substring(0, result.Length - padding);
        }

        static void Main(string[] args)
        {
            string test1 = "ICE ICE BABY\x04\x04\x04\x04";
            string test2 = "ICE ICE BABY\x05\x05\x05\x05";
            string test3 = "ICE ICE BABY\x01\x02\x03\x04";

            Console.WriteLine("\tTests\n");
            Console.WriteLine($"Test1: {PaddingValidation(test1)}");
            Console.WriteLine($"Test2: {PaddingValidation(test2)}");
            Console.WriteLine($"Test3: {PaddingValidation(test3)}");

            Console.ReadKey();
        }
    }
}
